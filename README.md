# Logback GELF UDP Appender.

Appender для Logback, поддерживающий отправку журнала в Graylog в формате GELF (TCP и UDP).

Основано на исходном коде библиотеки https://github.com/mp911de/logstash-gelf
