package ru.rcktsci.logging;

import org.junit.jupiter.api.Test;
import ru.rcktsci.logging.gelf.configuration.RuntimeContainer;
import ru.rcktsci.logging.gelf.configuration.RuntimeProperties;

import static org.assertj.core.api.Assertions.assertThat;

class RuntimeContainerUnitTests {

    @Test
    void testMain() {
        RuntimeContainer.main(new String[0]);
    }

    @Test
    void testDifferentOrder() {
        RuntimeProperties.setGelfHostnameResolutionOrder(RuntimeProperties.RESOLUTION_ORDER_LOCALHOST_NETWORK_FALLBACK);
        RuntimeContainer.lookupHostname(null);

        RuntimeProperties.setGelfHostnameResolutionOrder(RuntimeProperties.RESOLUTION_ORDER_NETWORK_LOCALHOST_FALLBACK);
        RuntimeContainer.lookupHostname(null);

        RuntimeProperties.setGelfHostnameResolutionOrder(RuntimeProperties.DEFAULT_UNKNOWN_PROPERTY);
    }

    @Test
    void testNoLookup() {

        RuntimeProperties.setGelfSkipHostnameResolution(true);
        RuntimeContainer.lookupHostname(null);

        assertThat(RuntimeContainer.ADDRESS).isEqualTo("");
        assertThat(RuntimeContainer.HOSTNAME).isEqualTo("unknown");
        assertThat(RuntimeContainer.FQDN_HOSTNAME).isEqualTo("unknown");

        RuntimeProperties.setGelfSkipHostnameResolution(false);
    }
}
