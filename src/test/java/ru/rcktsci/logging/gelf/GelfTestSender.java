package ru.rcktsci.logging.gelf;

import ru.rcktsci.logging.gelf.internal.GelfMessage;
import ru.rcktsci.logging.gelf.internal.GelfSender;

import java.util.ArrayList;
import java.util.List;

/**
 * @since 27.09.13 07:45
 */
public class GelfTestSender implements GelfSender {

    private static final List<GelfMessage> messages = new ArrayList<>();

    public static List<GelfMessage> getMessages() {
        return messages;
    }

    @Override
    public boolean sendMessage(GelfMessage message) {
        synchronized (messages) {
            messages.add(message);
        }
        return true;
    }

    @Override
    public void close() {

    }
}
