package ru.rcktsci.logging.gelf;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


class MessageFieldUnitTests {

    private static final String NAME = "name";

    @Test
    void testMdcMessageField() {
        MdcMessageField field = new MdcMessageField(NAME, "mdcName");
        assertThat(field.toString()).isEqualTo(MdcMessageField.class.getSimpleName() + " [name='name', mdcName='mdcName']");
    }

    @Test
    void testLogMessageField() {
        LogMessageField field = new LogMessageField(NAME, LogMessageField.NamedLogField.byName("SourceMethodName"));
        assertThat(field.toString())
                .isEqualTo(LogMessageField.class.getSimpleName() + " [name='name', namedLogField=SourceMethodName]");
    }

    @Test
    void testStaticMessageField() {
        StaticMessageField field = new StaticMessageField(NAME, "value");
        assertThat(field.toString()).isEqualTo(StaticMessageField.class.getSimpleName() + " [name='name', value='value']");
    }

    @Test
    void testDynamicMdcMessageField() {
        DynamicMdcMessageField field = new DynamicMdcMessageField(".*");
        assertThat(field.toString()).isEqualTo(DynamicMdcMessageField.class.getSimpleName() + " [regex='.*']");
    }

    @Test
    void testGetMapping() {
        List<LogMessageField> result = LogMessageField.getDefaultMapping(false, LogMessageField.NamedLogField.LoggerName,
                LogMessageField.NamedLogField.NDC);

        assertThat(result).hasSize(2);
    }

    @Test
    void testGetMappingAllFields() {

        List<LogMessageField> result = LogMessageField.getDefaultMapping(false, LogMessageField.NamedLogField.values());

        assertThat(result.size()).isEqualTo(LogMessageField.NamedLogField.values().length);
    }

    @Test
    void testGetMappingAllFieldsWithDefaultFile() {

        List<LogMessageField> result = LogMessageField.getDefaultMapping(true, LogMessageField.NamedLogField.values());

        assertThat(result.size()).isEqualTo(LogMessageField.NamedLogField.values().length);
    }
}
