package ru.rcktsci.logging.gelf;

import ru.rcktsci.logging.gelf.internal.GelfSender;
import ru.rcktsci.logging.gelf.internal.GelfSenderConfiguration;
import ru.rcktsci.logging.gelf.internal.GelfSenderProvider;

public class GelfTestSenderProvider implements GelfSenderProvider {

    @Override
    public boolean supports(String host) {
        return host.startsWith("test:");
    }

    @Override
    public GelfSender create(GelfSenderConfiguration configuration) {
        return new GelfTestSender();
    }

}
