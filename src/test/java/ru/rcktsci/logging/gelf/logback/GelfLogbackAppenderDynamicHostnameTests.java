package ru.rcktsci.logging.gelf.logback;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.MDC;

import ru.rcktsci.logging.gelf.GelfTestSender;
import ru.rcktsci.logging.gelf.internal.GelfMessage;

import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @since 2013-10-07
 */
class GelfLogbackAppenderDynamicHostnameTests {

    private static final String LOG_MESSAGE = "foo bar test log message";
    private LoggerContext lc;

    @BeforeEach
    void before() throws Exception {
        lc = new LoggerContext();
        JoranConfigurator configurator = new JoranConfigurator();
        configurator.setContext(lc);

        URL xmlConfigFile = getClass().getResource("/logback/logback-gelf-with-dynamic-originhost.xml");

        assertThat(xmlConfigFile).isNotNull();
        configurator.doConfigure(xmlConfigFile);

        GelfTestSender.getMessages().clear();

        MDC.remove("mdcField1");
    }

    @Test
    void testOriginHost() {

        Logger logger = lc.getLogger(getClass());

        logger.info(LOG_MESSAGE);
        assertThat(GelfTestSender.getMessages()).hasSize(1);

        GelfMessage gelfMessage = GelfTestSender.getMessages().get(0);

        String crossCheckHostName = gelfMessage.getAdditionalFields().get("crossCheckHostName");

        String json = gelfMessage.toJson();
        assertThat(json).contains("\"_crossCheckHostName\":\"" + crossCheckHostName + "\"");
        assertThat(json).contains("\"host\":\"" + crossCheckHostName + "\"");
    }
}
