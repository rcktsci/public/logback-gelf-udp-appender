package ru.rcktsci.logging.gelf.logback;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.MDC;

import ru.rcktsci.logging.gelf.GelfTestSender;

import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @since 2013-10-07
 */
class GelfLogbackAppenderTests extends AbstractGelfLogAppenderTests {

    @BeforeEach
    void before() throws Exception {
        lc = new LoggerContext();
        JoranConfigurator configurator = new JoranConfigurator();
        configurator.setContext(lc);

        URL xmlConfigFile = getClass().getResource("/logback/logback-gelf.xml");
        assertThat(xmlConfigFile).isNotNull();
        configurator.doConfigure(xmlConfigFile);

        GelfTestSender.getMessages().clear();

        MDC.remove("mdcField1");
    }
}
