package ru.rcktsci.logging.gelf.logback;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.MDC;

import ru.rcktsci.logging.gelf.GelfTestSender;
import ru.rcktsci.logging.gelf.internal.GelfMessage;

import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;


class GelfLogbackAppenderWithoutLocationTests {

    private static final String LOG_MESSAGE = "foo bar test log message";
    private LoggerContext lc = null;

    @BeforeEach
    void before() throws Exception {
        lc = new LoggerContext();
        JoranConfigurator configurator = new JoranConfigurator();
        configurator.setContext(lc);

        URL xmlConfigFile = getClass().getResource("/logback/logback-gelf-without-location.xml");
        assertThat(xmlConfigFile).isNotNull();
        configurator.doConfigure(xmlConfigFile);

        GelfTestSender.getMessages().clear();

        MDC.clear();
    }

    @Test
    void testWithoutLocation() {

        Logger logger = lc.getLogger(getClass());

        logger.info(LOG_MESSAGE);
        assertThat(GelfTestSender.getMessages()).hasSize(1);

        GelfMessage gelfMessage = GelfTestSender.getMessages().get(0);

        assertThat(gelfMessage.getField("SourceClassName")).isNull();
        assertThat(gelfMessage.getField("SourceSimpleClassName")).isNull();
        assertThat(gelfMessage.getField("SourceMethodName")).isNull();
        assertThat(gelfMessage.getField("SourceLineNumber")).isNull();
    }
}
