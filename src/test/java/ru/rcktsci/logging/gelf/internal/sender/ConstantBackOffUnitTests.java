package ru.rcktsci.logging.gelf.internal.sender;

import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Unit tests for {@link ConstantBackOff}.
 */
class ConstantBackOffUnitTests {

    @Test
    void shouldReturnConstantBackoff() {

        ConstantBackOff backOff = new ConstantBackOff(10, TimeUnit.SECONDS);

        assertThat(backOff.start().nextBackOff()).isEqualTo(TimeUnit.SECONDS.toMillis(10));
    }

}
