package ru.rcktsci.logging.gelf.internal.sender;

import org.junit.jupiter.api.Test;

import java.net.URI;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;


class QueryStringParserUnitTests {

    @Test
    void testParse() {
        Map<String, String> result = QueryStringParser.parse(URI.create("udp:12345?KeY=value"));
        assertThat(result).containsEntry("key", "value");
        assertThat(result).doesNotContainEntry("KeY", "value");
    }

    @Test
    void getHost() {
        assertThat(QueryStringParser.getHost(URI.create("udp:12345?KeY=value"))).isEqualTo("12345");
        assertThat(QueryStringParser.getHost(URI.create("udp:12345"))).isEqualTo("12345");
        assertThat(QueryStringParser.getHost(URI.create("udp://12345?KeY=value"))).isEqualTo("12345");
        assertThat(QueryStringParser.getHost(URI.create("udp://12345"))).isEqualTo("12345");
    }

    @Test
    void testGetTimeAsMsNoSuffix() {
        Map<String, String> map = QueryStringParser.parse(URI.create("udp:12345?timeout=1000"));
        long result = QueryStringParser.getTimeAsMs(map, "timeout", -1);
        assertThat(result).isEqualTo(1000);
    }

    @Test
    void testGetTimeAsMsNoSeconds() {
        Map<String, String> map = QueryStringParser.parse(URI.create("udp:12345?timeout=1s"));
        long result = QueryStringParser.getTimeAsMs(map, "timeout", -1);
        assertThat(result).isEqualTo(1000);
    }

    @Test
    void testGetTimeAsMsDefaultFallback() {
        Map<String, String> map = QueryStringParser.parse(URI.create("udp:12345?timeout=1s"));
        long result = QueryStringParser.getTimeAsMs(map, "not here", -1);
        assertThat(result).isEqualTo(-1);
    }

    @Test
    void testGetInt() {
        Map<String, String> map = QueryStringParser.parse(URI.create("udp:12345?timeout=1000"));
        int result = QueryStringParser.getInt(map, "timeout", -1);
        assertThat(result).isEqualTo(1000);
    }

    @Test
    void testGetIntDefault() {
        Map<String, String> map = QueryStringParser.parse(URI.create("udp:12345?timeout=1000"));
        int result = QueryStringParser.getInt(map, "not here", -1);
        assertThat(result).isEqualTo(-1);
    }
}
