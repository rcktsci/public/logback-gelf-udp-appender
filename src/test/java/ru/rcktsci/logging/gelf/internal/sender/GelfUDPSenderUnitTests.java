package ru.rcktsci.logging.gelf.internal.sender;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ru.rcktsci.logging.gelf.internal.ErrorReporter;
import ru.rcktsci.logging.gelf.internal.GelfMessage;

import java.net.DatagramSocket;
import java.util.Random;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

/**
 * Unit tests for {@link GelfUDPSender}.
 */
@ExtendWith(MockitoExtension.class)
class GelfUDPSenderUnitTests {

    @Mock
    private ErrorReporter errorReporter;

    @Test
    void unreachablePacketsShouldBeDiscardedSilently() throws Exception {

        GelfUDPSender udpSender = new GelfUDPSender("127.0.0.1", 65534, errorReporter);

        udpSender.sendMessage(new GelfMessage());

        verifyNoInteractions(errorReporter);
    }

    @Test
    void unknownHostShouldThrowException() throws Exception {

        new GelfUDPSender("unknown.host.unknown", 65534, errorReporter);
        verify(errorReporter).reportError(anyString(), any(Exception.class));
    }

    @Test
    void shouldSendDataToOpenPort() throws Exception {

        int port = randomPort();

        DatagramSocket socket = new DatagramSocket(port);

        GelfUDPSender udpSender = new GelfUDPSender("127.0.0.1", port, errorReporter);

        GelfMessage gelfMessage = new GelfMessage("short", "long", 1, "info");
        gelfMessage.setHost("host");

        udpSender.sendMessage(gelfMessage);

        GelfUDPSender spy = spy(udpSender);

        spy.sendMessage(gelfMessage);

        verify(spy).isConnected();
        verify(spy, never()).connect();

        socket.close();
        spy.close();
    }

    @Test
    void shouldSendDataToClosedPort() throws Exception {

        int port = randomPort();

        DatagramSocket socket = new DatagramSocket(port);

        GelfUDPSender udpSender = new GelfUDPSender("127.0.0.1", port, errorReporter);
        socket.close();

        GelfMessage gelfMessage = new GelfMessage("short", "long", 1, "info");
        gelfMessage.setHost("host");

        udpSender.sendMessage(gelfMessage);

        GelfUDPSender spy = spy(udpSender);
        doReturn(true).when(spy).isConnected();

        spy.sendMessage(gelfMessage);

        verify(spy).isConnected();
        verify(spy, never()).connect();

        spy.close();
    }

    int randomPort() {
        Random random = new Random();
        return random.nextInt(50000) + 1024;
    }

}
