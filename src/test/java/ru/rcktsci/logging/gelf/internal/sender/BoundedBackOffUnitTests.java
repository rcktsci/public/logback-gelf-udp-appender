package ru.rcktsci.logging.gelf.internal.sender;

import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Unit tests for {@link BoundedBackOff}.
 */
class BoundedBackOffUnitTests {

    final BoundedBackOff backOff = new BoundedBackOff(new ConstantBackOff(10, TimeUnit.SECONDS), 15, TimeUnit.SECONDS);

    @Test
    void shouldPassThruBackoff() {
        assertThat(backOff.start().nextBackOff()).isEqualTo(TimeUnit.SECONDS.toMillis(10));
    }

    @Test
    void shouldCapBackoff() {

        BackOffExecution backOffExecution = backOff.start();

        assertThat(backOffExecution.nextBackOff()).isEqualTo(TimeUnit.SECONDS.toMillis(10));
        assertThat(backOffExecution.nextBackOff()).isEqualTo(TimeUnit.SECONDS.toMillis(10));
        assertThat(backOffExecution.nextBackOff()).isEqualTo(BackOffExecution.STOP);
    }

}
