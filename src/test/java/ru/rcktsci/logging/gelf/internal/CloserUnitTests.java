package ru.rcktsci.logging.gelf.internal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.rcktsci.logging.gelf.utils.Closer;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
class CloserUnitTests {

    @Mock
    private InputStream inputStream;

    @Mock
    private Socket socket;

    @Test
    void closeSocketShouldClose() throws Exception {

        Closer.close(socket);

        verify(socket).close();
    }

    @Test
    void closeSocketDoesNotFailOnNull() {

        Closer.close(null);
    }

    @Test
    void closeSocketShouldNotPropagateExceptions() throws Exception {

        doThrow(new IOException()).when(socket).close();
        Closer.close(socket);
    }

    @Test
    void closeCloseableShouldClose() throws Exception {

        Closer.close(inputStream);

        verify(inputStream).close();
    }

    @Test
    void closeCloseableShouldNotPropagateExceptions() throws Exception {

        doThrow(new IOException()).when(inputStream).close();
        Closer.close(inputStream);
    }

    @Test
    void closeCloseableDoesNotFailOnNull() {

        Closer.close((Closeable) null);
    }
}
