package ru.rcktsci.logging.gelf.internal;

import org.junit.jupiter.api.Test;

import ru.rcktsci.logging.gelf.configuration.RuntimeProperties;
import ru.rcktsci.logging.gelf.internal.pools.DynamicPool;

import java.lang.reflect.Field;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;

import static org.assertj.core.api.Assertions.assertThat;
import static ru.rcktsci.logging.gelf.configuration.RuntimeProperties.DEFAULT_POOL_STATISTICS_VECTOR_SIZE;

public class DynamicPoolUnitTests {

    private static final int THREAD_TEST_COUNT = 1000;

    @Test
    void testDynamicPool() throws Exception {
        var pool = DynamicPool.create(Object::new);
        var obj = pool.getResource();

        assertThat(pool).isNotNull();

        pool.freeResource(obj);

        assertThat(getTypedQueue(pool).size()).isOne();
    }

    @Test
    void concurrentDynamicPoolTest() throws Exception {
        var pool = DynamicPool.create(Object::new);

        Thread[] threads = new Thread[THREAD_TEST_COUNT];

        for (int i = 0; i < THREAD_TEST_COUNT; i++) {
            threads[i] = new Thread(() -> {
                var resource = pool.getResource();
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ignored) {}
                pool.freeResource(resource);
            });
        }

        for (var thread : threads) {
            thread.start();
        }

        for (var thread : threads) {
            thread.join();
        }

        assertThat(getTypedQueue(pool).size()).isPositive();
    }

    @Test
    void testFreeResources() throws Exception {
        RuntimeProperties.setGelfDynamicPoolStatisticsSize(1);

        var pool = DynamicPool.create(Object::new);
        pool.freeResource(pool.getResource());

        ((AtomicInteger) getPrivateField(pool, "lastRangeMaxUsageCount").get(pool)).set(0);
        callPrivateMethodInPool(pool, "cleaner");

        assertThat(getTypedQueue(pool).size()).isZero();

        RuntimeProperties.setGelfDynamicPoolStatisticsSize(DEFAULT_POOL_STATISTICS_VECTOR_SIZE);
    }

    @Test
    void testContextResources() throws Exception {
        var pool = DynamicPool.create(Object::new);

        pool.executeWithinResourceContext(() -> {
            var contextResource = pool.getContextResource();
            assertThat(contextResource).isNotNull();
        });

        assertThat(getTypedQueue(pool).size()).isOne();
    }

    @Test
    void testChangedPropertiesInPool() throws Exception {
        // downscale statistics size
        var pool = preparePoolForChangingStats();

        RuntimeProperties.setGelfDynamicPoolStatisticsSize(2);

        callPrivateMethodInPool(pool, "runCleaner");
        var arr = ((long[]) getPrivateField(pool, "resourceUsageStatistics").get(pool));

        assertThat(arr).isEqualTo(new long[]{1L, 0L});

        // upscale statistics size
        pool = preparePoolForChangingStats();

        RuntimeProperties.setGelfDynamicPoolStatisticsSize(7);
        getPrivateField(pool, "resourceUsageStatisticsIndex").set(pool, 5);

        callPrivateMethodInPool(pool, "runCleaner");
        arr = ((long[]) getPrivateField(pool, "resourceUsageStatistics").get(pool));

        assertThat(arr).isEqualTo(new long[]{1L, 2L, 3L, 4L, 5L, 0L, -1L});

        // end
        RuntimeProperties.setGelfResourcePoolOptimizeTimeRange(5);
    }

    private DynamicPool<Object> preparePoolForChangingStats() throws Exception {
        RuntimeProperties.setGelfDynamicPoolStatisticsSize(5);
        RuntimeProperties.setGelfResourcePoolOptimizeTimeRange(1000);
        var pool = DynamicPool.create(Object::new);

        // Wait util thread cleaner will start
        Thread.sleep(50);

        long[] arr = ((long[]) getPrivateField(pool, "resourceUsageStatistics").get(pool));
        for (int i = 1; i < arr.length + 1; i++) {
            arr[i - 1] = i;
        }

        changeRuntimeProp("gelfResourcePoolOptimizeTimeRange", 0);

        return pool;
    }

    private static <T> Field getPrivateField(
            DynamicPool<T> pool,
            String name
    ) throws NoSuchFieldException {
        var field = pool.getClass().getDeclaredField(name);
        field.setAccessible(true);

        return field;
    }

    private void callPrivateMethodInPool(DynamicPool<?> pool, String method) throws Exception {
        var cleaner = pool.getClass().getDeclaredMethod(method);
        cleaner.setAccessible(true);
        cleaner.invoke(pool);
    }

    private <T> Queue<T> getTypedQueue(DynamicPool<T> pool) throws Exception {
        return (Queue<T>) getPrivateField(pool, "queue").get(pool);
    }

    private <T> void changeRuntimeProp(String key, T value) throws Exception {
        var field = RuntimeProperties.class.getDeclaredField(key);
        field.setAccessible(true);

        field.set(null, value);
    }
}
