package ru.rcktsci.logging.gelf.internal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import ru.rcktsci.logging.gelf.internal.pools.PoolHolder;
import ru.rcktsci.logging.gelf.utils.Closer;
import ru.rcktsci.logging.gelf.utils.StackTraceFilter;
import ru.rcktsci.logging.gelf.MdcGelfMessageAssembler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class GelfMessageIntegrationTests {

    private static final String FACILITY = "facility";
    private static final String VERSION = "2.0";
    private static final String FULL_MESSAGE = "full";
    private static final String SHORT_MESSAGE = "short";
    private static final String HOST = "localhost";
    private static final int PORT = 15000;
    private static final String LEVEL = "5";
    private static final long TIMESTAMP = 42;
    private static final int MESSAGE_SIZE = 5344;

    private static final Map<String, String> ADDITIONAL_FIELDS = new HashMap<>() {

        {
            put("a", "b");
            put("doubleNoDecimals", "2.0");
            put("doubleWithDecimals", "2.1");
            put("int", "2");
            put("exception1",
                StackTraceFilter.getFilteredStackTrace(new IOException(new Exception(new Exception()))));
            put("exception2",
                StackTraceFilter.getFilteredStackTrace(new IllegalStateException(new Exception(new Exception()))));
            put("exception3", StackTraceFilter
                    .getFilteredStackTrace(new IllegalArgumentException(new Exception(new IllegalArgumentException()))));
        }
    };

    @Test
    void testUdp() {

        GelfMessage gelfMessage = createGelfMessage();
        PoolingGelfMessage poolingGelfMessage = createPooledGelfMessage();

        ByteBuffer buffer = ByteBuffer.allocateDirect(2 * 8192);
        ByteBuffer buffer2 = ByteBuffer.allocateDirect(2 * 8192);

        ByteBuffer[] oldWay = gelfMessage.toUDPBuffers();
        ByteBuffer[] newWay = poolingGelfMessage.toUDPBuffers(buffer, buffer2);

        assertThat(newWay.length).isEqualTo(oldWay.length);

        for (int i = 0; i < oldWay.length; i++) {
            ByteBuffer oldChunk = oldWay[i];
            ByteBuffer newChunk = newWay[i];

            byte[] oldBytes = new byte[oldChunk.remaining()];
            byte[] newBytes = new byte[newChunk.remaining()];

            oldChunk.get(oldBytes);
            newChunk.get(newBytes);

            assertArrayEquals(newBytes, oldBytes);
        }
    }

    @Test
    void testUdpChunked() {

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 20000; i++) {
            int charId = (int) (Math.random() * Character.MAX_CODE_POINT);
            builder.append(charId);
        }

        GelfMessage gelfMessage = createGelfMessage();
        PoolingGelfMessage poolingGelfMessage = createPooledGelfMessage();

        gelfMessage.setFullMessage(builder.toString());
        poolingGelfMessage.setFullMessage(builder.toString());

        ByteBuffer buffer = ByteBuffer.allocateDirect(1200000);
        ByteBuffer tempBuffer = ByteBuffer.allocateDirect(60000);

        ByteBuffer[] oldWay = gelfMessage.toUDPBuffers();
        ByteBuffer[] newWay = poolingGelfMessage.toUDPBuffers(buffer, tempBuffer);

        assertThat(newWay.length).isEqualTo(oldWay.length);

        for (int i = 0; i < oldWay.length; i++) {

            ByteBuffer oldChunk = oldWay[i];
            ByteBuffer newChunk = newWay[i];

            byte[] oldBytes = new byte[oldChunk.remaining()];
            byte[] newBytes = new byte[newChunk.remaining()];

            oldChunk.get(oldBytes);
            newChunk.get(newBytes);

            assertArrayEquals(newBytes, oldBytes);
        }
    }

    @ParameterizedTest(name = "{index} case")
    @MethodSource("gelfMsgArgumentSource")
    void testSendUdpGelfMessage(GelfMessage msg) throws Exception {
        var sender = createGelfSender();

        asyncSendMessage(sender, msg);
        var resultPackage = receiveGelfMessage();

        Closer.close(sender);

        assertThat(resultPackage).isNotNull();

        var json = new String(resultPackage);
        // Exception depends from test environment, so i will ignore exception contents there
        assertThat(json).contains(
                "\"host\":\"localhost\",\"version\":\"2.0\",\"short_message\":\"short\",\"full_message\":\"full\",\"timestamp\":\"0.042\",\"level\":\"5\",\"facility\":\"facility\",\"_a\":\"b\",\"_doubleWithDecimals\":2.1",
                "\"_exception1\":\"");
    }

    private static Stream<Arguments> gelfMsgArgumentSource() {
        return Stream.of(
                Arguments.of(createGelfMessage()),
                Arguments.of(createPooledGelfMessage())
        );
    }

    private byte[] receiveGelfMessage() throws Exception {
        DatagramSocket ds = new DatagramSocket(PORT);
        byte[] bs = new byte[5000];
        DatagramPacket dp = new DatagramPacket(bs, bs.length);
        ds.receive(dp);

        var gzip = new GZIPInputStream(new ByteArrayInputStream(dp.getData()));
        byte[] bs1 = new byte[dp.getLength()];
        int result = gzip.read(bs1);

        ds.close();
        gzip.close();

        return result == -1 ? null : bs1;
    }

    private void asyncSendMessage(GelfSender sender, GelfMessage message) {
        new Thread(() -> {
            try {
                Thread.sleep(250);
            } catch (InterruptedException ignored) {
            }
            sender.sendMessage(message);
        }).start();
    }

    private GelfSender createGelfSender() {
        var gelfMessageAssembler = new MdcGelfMessageAssembler();
        gelfMessageAssembler.setHost(HOST);
        gelfMessageAssembler.setPort(PORT);

        return GelfSenderFactory.createSender(gelfMessageAssembler,
                                              new MessagePostprocessingErrorReporter(null),
                                              Collections.emptyMap());
    }

    private static GelfMessage createGelfMessage() {

        GelfMessage gelfMessage = new GelfMessage() {

            @Override
            long generateMsgId() {
                return 0x8040201008048683L;
            }
        };

        gelfMessage.setFacility(FACILITY);
        gelfMessage.setVersion(VERSION);
        gelfMessage.setFullMessage(FULL_MESSAGE);
        gelfMessage.setShortMessage(SHORT_MESSAGE);
        gelfMessage.setHost(HOST);
        gelfMessage.setJavaTimestamp(TIMESTAMP);
        gelfMessage.setLevel(LEVEL);
        gelfMessage.setMaximumMessageSize(MESSAGE_SIZE);
        gelfMessage.addFields(ADDITIONAL_FIELDS);
        return gelfMessage;
    }

    private static PoolingGelfMessage createPooledGelfMessage() {

        PoolingGelfMessage gelfMessage = new PoolingGelfMessage(PoolHolder.threadLocal()) {

            @Override
            long generateMsgId() {
                return 0x8040201008048683L;
            }
        };

        gelfMessage.setFacility(FACILITY);
        gelfMessage.setVersion(VERSION);
        gelfMessage.setFullMessage(FULL_MESSAGE);
        gelfMessage.setShortMessage(SHORT_MESSAGE);
        gelfMessage.setHost(HOST);
        gelfMessage.setJavaTimestamp(TIMESTAMP);
        gelfMessage.setLevel(LEVEL);
        gelfMessage.setMaximumMessageSize(MESSAGE_SIZE);
        gelfMessage.addFields(ADDITIONAL_FIELDS);
        return gelfMessage;
    }
}
