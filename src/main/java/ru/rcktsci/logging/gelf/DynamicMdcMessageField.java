package ru.rcktsci.logging.gelf;

import lombok.Getter;

import java.util.regex.Pattern;

/**
 * @since 28.02.14 09:56
 */
@Getter
public class DynamicMdcMessageField implements MessageField {

    private final String regex;
    private final Pattern pattern;

    public DynamicMdcMessageField(String regex) {
        this.regex = regex;
        this.pattern = Pattern.compile(regex);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() +
               " [regex='" + regex + '\'' +
               ']';
    }

    @Override
    public String name() {
        return null;
    }
}
