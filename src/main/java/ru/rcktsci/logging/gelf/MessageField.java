package ru.rcktsci.logging.gelf;

/**
 * Generic message field.
 */
public interface MessageField {

    /**
     * @return the field name.
     */
    String name();
}
