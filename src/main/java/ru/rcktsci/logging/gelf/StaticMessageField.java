package ru.rcktsci.logging.gelf;


public record StaticMessageField(String name, String value) implements MessageField {

    @Override
    public String toString() {
        return getClass().getSimpleName() +
                " [name='" + name + '\'' +
                ", value='" + value + '\'' +
                ']';
    }
}
