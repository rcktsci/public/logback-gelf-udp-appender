package ru.rcktsci.logging.gelf;

import ru.rcktsci.logging.gelf.configuration.RuntimeContainer;
import ru.rcktsci.logging.gelf.configuration.RuntimeProperties;
import ru.rcktsci.logging.gelf.utils.StackTraceFilter;
import ru.rcktsci.logging.gelf.internal.GelfMessage;
import ru.rcktsci.logging.gelf.internal.HostAndPortProvider;
import ru.rcktsci.logging.gelf.internal.PoolingGelfMessageBuilder;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import static ru.rcktsci.logging.gelf.GelfMessageBuilder.newInstance;

/**
 * Creates {@link GelfMessage} based on various {@link LogEvent}. A {@link LogEvent} encapsulates log-framework specifics and
 * exposes commonly used details of log events.
 *
 * @since 26.09.13 15:05
 */
public class GelfMessageAssembler implements HostAndPortProvider {

    public static final String FIELD_MESSAGE_PARAM = "MessageParam";
    public static final String FIELD_STACK_TRACE = "StackTrace";

    private static final int MAX_SHORT_MESSAGE_LENGTH = 250;
    private static final int MAX_PORT_NUMBER = 65535;
    private static final int MAX_MESSAGE_SIZE = Integer.MAX_VALUE;
    private static final Set<LogMessageField.NamedLogField> SOURCE_FIELDS = EnumSet.of(LogMessageField.NamedLogField.SourceClassName,
            LogMessageField.NamedLogField.SourceSimpleClassName, LogMessageField.NamedLogField.SourceMethodName, LogMessageField.NamedLogField.SourceLineNumber);
    private final ThreadLocal<PoolingGelfMessageBuilder> poolingBuilder;
    private final List<MessageField> fields = new ArrayList<>();
    private final Map<String, String> additionalFieldTypes = new HashMap<>();
    private final Map<Pattern, String> dynamicMdcFieldTypes = new LinkedHashMap<>();
    private String host;
    private String version = GelfMessage.GELF_VERSION;
    private String originHost;
    private int port;
    private String facility;
    private boolean includeLogMessageParameters = true;
    private boolean includeLocation = true;
    private StackTraceExtraction stackTraceExtraction = StackTraceExtraction.OFF;
    private int maximumMessageSize = 8192;
    private String timestampPattern = "yyyy-MM-dd HH:mm:ss,SSS";

    public GelfMessageAssembler() {
        poolingBuilder = ThreadLocal.withInitial(PoolingGelfMessageBuilder::newInstance);
    }

    /**
     * Initialize the {@link GelfMessageAssembler} from a property provider.
     *
     * @param propertyProvider property provider to obtain configuration properties
     */
    public void initialize(PropertyProvider propertyProvider) {

        host = propertyProvider.getProperty(PropertyProvider.PROPERTY_HOST);
        if (host == null) {
            host = propertyProvider.getProperty(PropertyProvider.PROPERTY_GRAYLOG_HOST);
        }

        String port = propertyProvider.getProperty(PropertyProvider.PROPERTY_PORT);
        if (port == null) {
            port = propertyProvider.getProperty(PropertyProvider.PROPERTY_GRAYLOG_PORT);
        }

        if (port != null && !"".equals(port)) {
            this.port = Integer.parseInt(port);
        }

        originHost = propertyProvider.getProperty(PropertyProvider.PROPERTY_ORIGIN_HOST);
        setExtractStackTrace(propertyProvider.getProperty(PropertyProvider.PROPERTY_EXTRACT_STACKTRACE));
        setFilterStackTrace("true".equalsIgnoreCase(propertyProvider.getProperty(PropertyProvider.PROPERTY_FILTER_STACK_TRACE)));

        String includeLogMessageParameters = propertyProvider
                .getProperty(PropertyProvider.PROPERTY_INCLUDE_LOG_MESSAGE_PARAMETERS);
        if (includeLogMessageParameters != null && !includeLogMessageParameters.trim().equals("")) {
            setIncludeLogMessageParameters("true".equalsIgnoreCase(includeLogMessageParameters));
        }

        setupStaticFields(propertyProvider);
        setupAdditionalFieldTypes(propertyProvider);
        setupDynamicMdcFieldTypes(propertyProvider);
        facility = propertyProvider.getProperty(PropertyProvider.PROPERTY_FACILITY);
        String version = propertyProvider.getProperty(PropertyProvider.PROPERTY_VERSION);

        if (version != null && !"".equals(version)) {
            this.version = version;
        }

        String messageSize = propertyProvider.getProperty(PropertyProvider.PROPERTY_MAX_MESSAGE_SIZE);
        if (messageSize != null) {
            maximumMessageSize = Integer.parseInt(messageSize);
        }

        String timestampPattern = propertyProvider.getProperty(PropertyProvider.PROPERTY_TIMESTAMP_PATTERN);
        if (timestampPattern != null && !"".equals(timestampPattern)) {
            this.timestampPattern = timestampPattern;
        }
    }

    /**
     * Produce a {@link GelfMessage}.
     *
     * @param logEvent the log event
     * @return a new GelfMessage
     */
    public GelfMessage createGelfMessage(LogEvent logEvent) {
        var isPoolingEnabled = RuntimeProperties.getCurrentMessagePoolingFlag().isEnabled();

        GelfMessageBuilder builder = isPoolingEnabled ? poolingBuilder.get().recycle() : newInstance();

        Throwable throwable = logEvent.getThrowable();
        String message = logEvent.getMessage();

        if (GelfMessage.isEmpty(message) && throwable != null) {
            message = throwable.toString();
        }

        String shortMessage = message;
        if (message.length() > MAX_SHORT_MESSAGE_LENGTH) {
            shortMessage = message.substring(0, MAX_SHORT_MESSAGE_LENGTH - 1);
        }

        builder.withShortMessage(shortMessage).withFullMessage(message).withJavaTimestamp(logEvent.getLogTimestamp());
        builder.withLevel(logEvent.getSyslogLevel());
        builder.withVersion(getVersion());
        builder.withAdditionalFieldTypes(additionalFieldTypes);
        builder.withDynamicMdcFieldTypes(dynamicMdcFieldTypes);

        for (MessageField field : fields) {

            if (!isIncludeLocation() && field instanceof LogMessageField messageField) {

                if (SOURCE_FIELDS.contains(messageField.namedLogField())) {
                    continue;
                }
            }

            Values values = getValues(logEvent, field);
            if (values == null || !values.hasValues()) {
                continue;
            }

            for (String entryName : values.getEntryNames()) {
                String value = values.getValue(entryName);
                if (value == null) {
                    continue;

                }
                builder.withField(entryName, value);
            }
        }

        if (stackTraceExtraction.isEnabled() && throwable != null) {
            addStackTrace(throwable, builder);
        }

        if (includeLogMessageParameters && logEvent.getParameters() != null) {
            for (int i = 0; i < logEvent.getParameters().length; i++) {
                Object param = logEvent.getParameters()[i];
                builder.withField(FIELD_MESSAGE_PARAM + i, "" + param);
            }
        }

        builder.withHost(getOriginHost());

        if (null != facility) {
            builder.withFacility(facility);
        }

        builder.withMaximumMessageSize(maximumMessageSize);
        return builder.build();
    }

    private Values getValues(LogEvent logEvent, MessageField field) {

        if (field instanceof StaticMessageField smf) {
            return new Values(field.name(), getValue(smf));
        }

        if (field instanceof LogMessageField logMessageField) {
            if (logMessageField.namedLogField() == LogMessageField.NamedLogField.Time) {
                SimpleDateFormat dateFormat = new SimpleDateFormat(timestampPattern);
                return new Values(field.name(), dateFormat.format(new Date(logEvent.getLogTimestamp())));
            }

            if (logMessageField.namedLogField() == LogMessageField.NamedLogField.Server) {
                return new Values(field.name(), getOriginHost());
            }
        }

        return logEvent.getValues(field);
    }

    private String getValue(StaticMessageField field) {
        return field.value();
    }

    private void addStackTrace(Throwable thrown, GelfMessageBuilder builder) {
        if (stackTraceExtraction.isFilter()) {
            builder.withField(FIELD_STACK_TRACE, StackTraceFilter.getFilteredStackTrace(thrown, stackTraceExtraction.getRef()));
        } else {
            final StringWriter sw = new StringWriter();
            StackTraceFilter.getThrowable(thrown, stackTraceExtraction.getRef()).printStackTrace(new PrintWriter(sw));
            builder.withField(FIELD_STACK_TRACE, sw.toString());
        }
    }

    private void setupStaticFields(PropertyProvider propertyProvider) {
        int fieldNumber = 0;
        while (true) {
            final String property = propertyProvider.getProperty(PropertyProvider.PROPERTY_ADDITIONAL_FIELD + fieldNumber);
            if (null == property) {
                break;
            }
            final int index = property.indexOf('=');
            if (-1 != index) {

                StaticMessageField field = new StaticMessageField(property.substring(0, index), property.substring(index + 1));
                addField(field);
            }

            fieldNumber++;
        }
    }

    private void setupAdditionalFieldTypes(PropertyProvider propertyProvider) {
        int fieldNumber = 0;
        while (true) {
            final String property = propertyProvider.getProperty(PropertyProvider.PROPERTY_ADDITIONAL_FIELD_TYPE + fieldNumber);
            if (null == property) {
                break;
            }
            final int index = property.indexOf('=');
            if (-1 != index) {

                String field = property.substring(0, index);
                String type = property.substring(index + 1);
                setAdditionalFieldType(field, type);
            }

            fieldNumber++;
        }
    }

    private void setupDynamicMdcFieldTypes(PropertyProvider propertyProvider) {
        int fieldNumber = 0;
        while (true) {
            String property = propertyProvider.getProperty(PropertyProvider.PROPERTY_DYNAMIC_MDC_FIELD_TYPES + fieldNumber);
            if (null == property) {
                break;
            }
            int index = property.indexOf('=');
            if (-1 != index) {

                String field = property.substring(0, index);
                String type = property.substring(index + 1);
                setDynamicMdcFieldType(field, type);
            }

            fieldNumber++;
        }
    }

    public void setAdditionalFieldType(String field, String type) {
        additionalFieldTypes.put(field, type);
    }

    public void setDynamicMdcFieldType(String fieldPattern, String type) {
        Pattern pattern = Pattern.compile(fieldPattern);
        setDynamicMdcFieldType(pattern, type);
    }

    public void setDynamicMdcFieldType(Pattern fieldPattern, String type) {
        dynamicMdcFieldTypes.put(fieldPattern, type);
    }

    public void addField(MessageField field) {
        if (!fields.contains(field)) {
            this.fields.add(field);
        }
    }

    public void addFields(Collection<? extends MessageField> fields) {
        this.fields.addAll(fields);
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getOriginHost() {
        if (null == originHost) {
            originHost = RuntimeContainer.FQDN_HOSTNAME;
        }
        return originHost;
    }

    public void setOriginHost(String originHost) {
        this.originHost = originHost;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        if (port > MAX_PORT_NUMBER || port < 1) {
            throw new IllegalArgumentException("Invalid port number: " + port + ", supported range: 1-" + MAX_PORT_NUMBER);
        }
        this.port = port;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public boolean isExtractStackTrace() {
        return stackTraceExtraction.isEnabled();
    }

    public String getExtractStackTrace() {

        if (stackTraceExtraction.isEnabled()) {

            if (stackTraceExtraction.getRef() == 0) {
                return "true";
            }
            return Integer.toString(stackTraceExtraction.getRef());
        }

        return "false";
    }

    public void setExtractStackTrace(boolean extractStackTrace) {
        this.stackTraceExtraction = stackTraceExtraction.applyExtraction("" + extractStackTrace);
    }

    public void setExtractStackTrace(String value) {
        this.stackTraceExtraction = stackTraceExtraction.applyExtraction(value);
    }

    public boolean isFilterStackTrace() {
        return stackTraceExtraction.isEnabled();
    }

    public void setFilterStackTrace(boolean filterStackTrace) {
        this.stackTraceExtraction = stackTraceExtraction.applyFilter(filterStackTrace);
    }

    public boolean isIncludeLogMessageParameters() {
        return includeLogMessageParameters;
    }

    public void setIncludeLogMessageParameters(boolean includeLogMessageParameters) {
        this.includeLogMessageParameters = includeLogMessageParameters;
    }

    public boolean isIncludeLocation() {
        return includeLocation;
    }

    public void setIncludeLocation(boolean includeLocation) {
        this.includeLocation = includeLocation;
    }

    public String getTimestampPattern() {
        return timestampPattern;
    }

    public void setTimestampPattern(String timestampPattern) {
        this.timestampPattern = timestampPattern;
    }

    public int getMaximumMessageSize() {
        return maximumMessageSize;
    }

    public void setMaximumMessageSize(int maximumMessageSize) {

        if (maximumMessageSize < 1) {
            throw new IllegalArgumentException(
                    "Invalid maximum message size: " + maximumMessageSize + ", supported range: 1-" + MAX_MESSAGE_SIZE);
        }

        this.maximumMessageSize = maximumMessageSize;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {

        if (!GelfMessage.GELF_VERSION_1_0.equals(version) && !GelfMessage.GELF_VERSION_1_1.equals(version)) {
            throw new IllegalArgumentException("Invalid GELF version: " + version + ", supported range: "
                    + GelfMessage.GELF_VERSION_1_0 + ", " + GelfMessage.GELF_VERSION_1_1);
        }

        this.version = version;
    }

    static class StackTraceExtraction {

        private static final StackTraceExtraction OFF = new StackTraceExtraction(false, false, 0);
        private static final StackTraceExtraction ON = new StackTraceExtraction(true, false, 0);
        private static final StackTraceExtraction FILTERED = new StackTraceExtraction(true, true, 0);
        private final boolean enabled;
        private final boolean filter;
        private final int ref;

        private StackTraceExtraction(boolean enabled, boolean filter, int ref) {
            this.enabled = enabled;
            this.filter = filter;
            this.ref = ref;
        }

        /**
         * Parse the stack trace filtering value.
         *
         * @param value
         * @return
         */
        public static StackTraceExtraction from(String value, boolean filter) {

            if (value == null) {
                return OFF;
            }

            boolean enabled = Boolean.parseBoolean(value);

            int ref = 0;
            if (!value.equalsIgnoreCase("false") && !value.trim().isEmpty()) {
                try {
                    ref = Integer.parseInt(value);
                    enabled = true;
                } catch (NumberFormatException ignored) {
                }
            }

            return new StackTraceExtraction(enabled, filter, ref);
        }

        public StackTraceExtraction applyExtraction(String value) {

            StackTraceExtraction parsed = from(value, isFilter());
            return new StackTraceExtraction(parsed.isEnabled(), parsed.isFilter(), parsed.getRef());
        }

        public StackTraceExtraction applyFilter(boolean filterStackTrace) {
            return new StackTraceExtraction(isEnabled(), filterStackTrace, getRef());
        }

        public boolean isEnabled() {
            return enabled;
        }

        public boolean isFilter() {
            return filter;
        }

        public int getRef() {
            return ref;
        }
    }
}
