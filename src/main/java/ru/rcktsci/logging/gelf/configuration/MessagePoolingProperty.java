package ru.rcktsci.logging.gelf.configuration;

public enum MessagePoolingProperty {
    TRUE,
    FALSE,
    STATIC;

    public boolean isStaticProperty() {
        return this == STATIC;
    }

    public boolean isEnabled() {
        return this != FALSE;
    }
}
