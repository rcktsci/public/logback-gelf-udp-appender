package ru.rcktsci.logging.gelf.configuration;

import lombok.Getter;
import lombok.Setter;

import ru.rcktsci.logging.gelf.internal.pools.DynamicPool;

/**
 * Collection of properties to control host name/host name resolution, logging rules and pooling of gelf messages.
 */
public class RuntimeProperties {

    /**
     * Just in time current message pooling property value.
     */
    private static final ThreadLocal<MessagePoolingProperty> currentMessagePooling = ThreadLocal.withInitial(
            () -> MessagePoolingProperty.FALSE
    );

    /**
     * Buffer size for transmit buffers. Defaults to 40 * 8192
     */
    public static final Integer DEFAULT_POOL_BUFFER_SIZE = 40 * 8192;

    /**
     * Default number of intervals taken into statistics when calculating statistics for managing resource pools when sending a message
     */
    public static final Integer DEFAULT_POOL_STATISTICS_VECTOR_SIZE = 5;

    /**
     * Default value of time interval of checking and optimizing the pool holder in seconds
     */
    public static final Integer DEFAULT_POOL_STATISTICS_TIME_RANGE = 60;

    /**
     * Resolution order: First inspect the local host name, then try to get the host name from network devices.
     */
    public static final String RESOLUTION_ORDER_LOCALHOST_NETWORK_FALLBACK = "localhost,network";

    /**
     * Resolution order: First inspect the network devices to retrieve a host name, then try to get the host name from the local
     * host.
     */
    public static final String RESOLUTION_ORDER_NETWORK_LOCALHOST_FALLBACK = "network,localhost";

    /**
     * Default value of gelf message pooling.
     */
    public static final MessagePoolingProperty DEFAULT_MESSAGE_POOLING = MessagePoolingProperty.TRUE;

    /**
     * Default value of {@link #gelfHostnameProperty} and {@link #gelfFQDNProperty}
     */
    public static final String DEFAULT_UNKNOWN_PROPERTY = "unknown";

    /**
     * Set this property to change pooling of Gelf messages property
     * <p>
     * Can be
     * <ul>
     * <li>{@literal static} (default value) for static held pools</li>
     * <li>{@literal true} for using instance-based held pools</li>
     * <li>{@literal false} to disable pooling</li>
     * </ul>
     */
    @Getter
    @Setter
    public static MessagePoolingProperty gelfMessagePoolingProperty;

    @Getter
    @Setter
    public static int bufferSizeProperty;

    /**
     * Set this property to enable the verbose logging
     */
    @Getter
    @Setter
    private static boolean verboseLoggingProperty;

    /**
     * Set this property to a simple hostname to use a fixed host name.
     */
    @Getter
    @Setter
    private static String gelfHostnameProperty;

    /**
     * Set this property to a fully qualified hostname to use a fixed host name.
     */
    @Getter
    @Setter
    private static String gelfFQDNProperty;

    /**
     * Set this property to <code>true</code> to skip hostname resolution. The string <code>unknown</code> will be used as
     * hostname.
     */
    @Getter
    @Setter
    private static boolean gelfSkipHostnameResolution;

    /**
     * Set this value to change the number of intervals taken into statistics when calculating statistics for managing resource pools when sending a message.
     * Must be greater than zero.
     * <p>
     * See {@link DynamicPool}
     */
    @Getter
    private static int gelfDynamicPoolStatisticsSize;

    public static void setGelfDynamicPoolStatisticsSize(int newSize) {
        if (newSize <= 0) {
            throw new IllegalArgumentException(
                    "gelfDynamicPoolStatisticsSize must be greater than zero!");
        }
        gelfDynamicPoolStatisticsSize = newSize;
    }

    /**
     * Set this value to change the time interval of checking and optimizing the pool holder.
     * Measured in seconds. Must be greater than zero.
     * <p>
     * See {@link DynamicPool}
     */
    @Getter
    private static int gelfResourcePoolOptimizeTimeRange;

    public static void setGelfResourcePoolOptimizeTimeRange(Integer newRange) {
        if (newRange <= 0) {
            throw new IllegalArgumentException(
                    "gelfResourcePoolOptimizeTimeRange must be greater than zero!");
        }
        gelfResourcePoolOptimizeTimeRange = newRange;
    }

    /**
     * Set this property to {@link #RESOLUTION_ORDER_LOCALHOST_NETWORK_FALLBACK} or
     * {@link #RESOLUTION_ORDER_NETWORK_LOCALHOST_FALLBACK} to control the hostname resolution order.
     */
    @Getter
    @Setter
    private static String gelfHostnameResolutionOrder;

    static {
        setBufferSizeProperty(Integer.parseInt(getSystemProperty("logback-gelf.buffer.size",
                                                                 "" + DEFAULT_POOL_BUFFER_SIZE)));
        setVerboseLoggingProperty(Boolean.parseBoolean(getSystemProperty(
                "logback-gelf.LogMessageField.verbose",
                "false")));
        setGelfHostnameProperty(getSystemProperty("logback-gelf.hostname",
                                                  DEFAULT_UNKNOWN_PROPERTY));
        setGelfFQDNProperty(getSystemProperty("logback-gelf.fqdn.hostname",
                                              DEFAULT_UNKNOWN_PROPERTY));
        setGelfSkipHostnameResolution(Boolean.parseBoolean(getSystemProperty(
                "logback-gelf.skipHostnameResolution",
                "false")));
        setGelfHostnameResolutionOrder(getSystemProperty("logback-gelf.resolutionOrder",
                                                         RESOLUTION_ORDER_NETWORK_LOCALHOST_FALLBACK));
        setGelfDynamicPoolStatisticsSize(Integer.parseInt(getSystemProperty(
                "logback-gelf.message.pooling.stat-size",
                "" + DEFAULT_POOL_STATISTICS_VECTOR_SIZE)));
        setGelfResourcePoolOptimizeTimeRange(Integer.valueOf(getSystemProperty(
                "logback-gelf.message.pooling.opt-range",
                "" + DEFAULT_POOL_STATISTICS_TIME_RANGE)));

        var gelfMessagePoolingPropertyStr = assertMessagePoolingPropertyIsCorrect(
                getSystemProperty(
                        "logback-gelf.message.pooling",
                        DEFAULT_MESSAGE_POOLING.name().toLowerCase()
                )
        );
        setGelfMessagePoolingProperty(MessagePoolingProperty.valueOf(gelfMessagePoolingPropertyStr.toUpperCase()));
    }

    public static MessagePoolingProperty getCurrentMessagePoolingFlag() {
        return currentMessagePooling.get();
    }

    public static void updateTLMessagePoolingFlag() {
        currentMessagePooling.set(gelfMessagePoolingProperty);
    }

    private static String assertMessagePoolingPropertyIsCorrect(String newVal) {
        if (!checkGelfMessagePoolingProperty(newVal)) {
            throw new IllegalArgumentException("Undefined value for gelfMessagePoolingProperty: " + newVal);
        }
        return newVal;
    }

    private static boolean checkGelfMessagePoolingProperty(String prop) {
        return switch (prop.toLowerCase()) {
            case "true", "false", "static" -> true;
            default -> false;
        };
    }

    private static String getSystemProperty(String key, String defaultValue) {
        String env = System.getenv(key);
        if (env != null && !"".equals(env)) {
            return env;
        }
        return System.getProperty(key, defaultValue);
    }
}
