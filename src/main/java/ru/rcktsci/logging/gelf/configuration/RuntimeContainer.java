package ru.rcktsci.logging.gelf.configuration;

import ru.rcktsci.logging.gelf.internal.ErrorReporter;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import static ru.rcktsci.logging.gelf.configuration.RuntimeProperties.RESOLUTION_ORDER_LOCALHOST_NETWORK_FALLBACK;
import static ru.rcktsci.logging.gelf.configuration.RuntimeProperties.RESOLUTION_ORDER_NETWORK_LOCALHOST_FALLBACK;

/**
 * Static Details about the runtime container: Hostname (simple/fqdn), Address and timestamp of the first access (time when the
 * application was loaded).
 */
public class RuntimeContainer {

    /**
     * Load-Time of this class.
     */
    public static final long FIRST_ACCESS;
    /**
     * Current Hostname.
     */
    public static String HOSTNAME;
    /**
     * Current FQDN Hostname.
     */
    public static String FQDN_HOSTNAME;
    /**
     * Current Address.
     */
    public static String ADDRESS;
    private static boolean initialized;

    static {
        FIRST_ACCESS = System.currentTimeMillis();
    }

    /**
     * Utility Constructor.
     */
    private RuntimeContainer() {

    }

    /**
     * Initialize only once.
     *
     * @param errorReporter the error reporter
     */
    public static void initialize(ErrorReporter errorReporter) {

        if (!initialized) {
            lookupHostname(errorReporter);
            initialized = true;
        }
    }

    /**
     * Triggers the hostname lookup.
     *
     * @param errorReporter the error reporter
     */
    public static void lookupHostname(ErrorReporter errorReporter) {
        String myHostName = RuntimeProperties.getGelfHostnameProperty();
        String myFQDNHostName = RuntimeProperties.getGelfFQDNProperty();
        String myAddress = "";

        if (!RuntimeProperties.isGelfSkipHostnameResolution()) {

            try {

                String resolutionOrder = RuntimeProperties.getGelfHostnameResolutionOrder();

                InetAddress inetAddress = null;
                if (resolutionOrder.equals(RESOLUTION_ORDER_NETWORK_LOCALHOST_FALLBACK)) {
                    inetAddress = getInetAddressWithHostname();
                }

                if (resolutionOrder.equals(RESOLUTION_ORDER_LOCALHOST_NETWORK_FALLBACK)) {
                    if (isQualified(InetAddress.getLocalHost())) {
                        inetAddress = InetAddress.getLocalHost();
                    } else {
                        inetAddress = getInetAddressWithHostname();
                    }
                }

                if (inetAddress == null) {
                    inetAddress = InetAddress.getLocalHost();
                }

                myHostName = getHostname(inetAddress, false);
                myFQDNHostName = getHostname(inetAddress, true);
                myAddress = inetAddress.getHostAddress();
            } catch (IOException e) {
                errorReporter.reportError("Cannot resolve hostname", e);
            }
        }

        FQDN_HOSTNAME = myFQDNHostName;
        HOSTNAME = myHostName;
        ADDRESS = myAddress;
    }

    private static String getHostname(InetAddress inetAddress, boolean fqdn) throws IOException {

        String hostname = inetAddress.getHostName();
        if (hostname.indexOf('.') != -1 && !fqdn) {
            hostname = hostname.substring(0, hostname.indexOf('.'));
        }

        return hostname;
    }

    private static InetAddress getInetAddressWithHostname() throws SocketException {
        Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();

        while (netInterfaces.hasMoreElements()) {
            NetworkInterface ni = netInterfaces.nextElement();
            Enumeration<InetAddress> ias = ni.getInetAddresses();
            while (ias.hasMoreElements()) {
                InetAddress inetAddress = ias.nextElement();

                if (!isQualified(inetAddress)) {
                    continue;
                }
                return inetAddress;
            }
        }

        return null;
    }

    private static boolean isQualified(InetAddress inetAddress) {
        return !inetAddress.isLoopbackAddress() && !inetAddress.getHostAddress().equals(inetAddress.getCanonicalHostName());
    }

    public static void main(String[] args) {
        System.out.println("Host-Resolution: " + FQDN_HOSTNAME + "/" + HOSTNAME);
    }
}
