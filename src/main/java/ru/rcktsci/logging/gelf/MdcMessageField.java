package ru.rcktsci.logging.gelf;


public record MdcMessageField(String name, String mdcName) implements MessageField {

    @Override
    public String toString() {
        return getClass().getSimpleName() +
                " [name='" + name + '\'' +
                ", mdcName='" + mdcName + '\'' +
                ']';
    }
}
