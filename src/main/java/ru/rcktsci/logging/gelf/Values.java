package ru.rcktsci.logging.gelf;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Value object to abstract multiple values mapped by a {@link String} key.
 *
 * @since 28.02.14 09:50
 */
public class Values {

    private final Map<String, Object> values = new HashMap<>();

    public Values() {
    }

    public Values(String name, Object value) {
        if (name != null && value != null) {
            values.put(name, value);
        }
    }

    public boolean hasValues() {
        return !values.isEmpty();
    }

    public int size() {
        return values.size();
    }

    public Set<String> getEntryNames() {
        return Collections.unmodifiableSet(values.keySet());
    }

    public void setValue(String key, Object value) {
        values.put(key, value);
    }

    public String getValue(String key) {
        return String.valueOf(values.get(key));
    }
}
