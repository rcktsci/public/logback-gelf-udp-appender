package ru.rcktsci.logging.gelf.internal;

import ru.rcktsci.logging.gelf.GelfMessageBuilder;
import ru.rcktsci.logging.gelf.configuration.RuntimeProperties;
import ru.rcktsci.logging.gelf.internal.pools.DynamicPoolStore;
import ru.rcktsci.logging.gelf.internal.pools.PoolHolder;

public class PoolingGelfMessageBuilder extends GelfMessageBuilder {

    private static final PoolHolder staticPoolHolder = PoolHolder.threadLocal();

    /**
     * Creates a new instance of the GelfMessageBuilder.
     *
     * @return GelfMessageBuilder
     */
    public static PoolingGelfMessageBuilder newInstance() {
        return new PoolingGelfMessageBuilder();
    }

    /**
     * Recycle this {@link GelfMessageBuilder} to a default state.
     *
     * @return {@code this} {@link GelfMessageBuilder}
     */
    public GelfMessageBuilder recycle() {
        version = GelfMessage.GELF_VERSION;
        host = null;
        shortMessage = null;
        fullMessage = null;
        javaTimestamp = 0;
        level = null;
        facility = GelfMessage.DEFAULT_FACILITY;
        maximumMessageSize = GelfMessage.DEFAULT_MESSAGE_SIZE;

        additionalFields.clear();
        additionalFieldTypes.clear();

        return this;
    }

    /**
     * Build a new Gelf message based on the builder settings.
     *
     * @return GelfMessage
     */
    @Override
    public GelfMessage build() {
        var holder = RuntimeProperties.getCurrentMessagePoolingFlag().isStaticProperty()
                     ? staticPoolHolder
                     : DynamicPoolStore.getDynamicPoolHolder().getContextResource();

        GelfMessage gelfMessage = new PoolingGelfMessage(shortMessage,
                                                         fullMessage,
                                                         javaTimestamp,
                                                         level,
                                                         holder);
        fillMsg(gelfMessage);

        return gelfMessage;
    }
}
