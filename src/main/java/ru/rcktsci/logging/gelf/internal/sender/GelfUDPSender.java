package ru.rcktsci.logging.gelf.internal.sender;

import ru.rcktsci.logging.gelf.configuration.RuntimeProperties;
import ru.rcktsci.logging.gelf.utils.Closer;
import ru.rcktsci.logging.gelf.internal.pools.DynamicPoolStore;
import ru.rcktsci.logging.gelf.internal.ErrorReporter;
import ru.rcktsci.logging.gelf.internal.GelfMessage;
import ru.rcktsci.logging.gelf.internal.GelfSender;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.UnresolvedAddressException;

public class GelfUDPSender extends AbstractNioSender<DatagramChannel> implements GelfSender {

    private final Object ioLock = new Object();

    public GelfUDPSender(String host, int port, ErrorReporter errorReporter) throws IOException {
        super(errorReporter, host, port);
        connect();
    }

    public boolean sendMessage(GelfMessage message) {

        if (RuntimeProperties.getBufferSizeProperty() == 0) {
            return sendDatagrams(message.toUDPBuffers());
        }

        var writeBuffers = DynamicPoolStore.getDynamicByteBufferPool().getResource();
        var tempBuffers = DynamicPoolStore.getDynamicByteBufferPool().getResource();

        var result = sendDatagrams(GelfBuffers.toUDPBuffers(message, writeBuffers.data(), tempBuffers.data()));

        DynamicPoolStore.getDynamicByteBufferPool().freeResource(writeBuffers);
        DynamicPoolStore.getDynamicByteBufferPool().freeResource(tempBuffers);

        return result;
    }

    private boolean sendDatagrams(ByteBuffer[] bytesList) {

        try {
            // (re)-connect if necessary
            if (!isConnected()) {
                synchronized (ioLock) {
                    connect();
                }
            }

            for (ByteBuffer buffer : bytesList) {

                synchronized (ioLock) {
                    while (buffer.hasRemaining()) {
                        channel().write(buffer);
                    }
                }
            }
        } catch (IOException e) {
            reportError(e.getMessage(), new IOException("Cannot send data to " + getHost() + ":" + getPort(), e));
            return false;
        }

        return true;
    }

    protected void connect() throws IOException {

        if (isConnected()) {
            return;
        }

        if (channel() == null) {
            setChannel(DatagramChannel.open());
        } else if (!channel().isOpen()) {
            Closer.close(channel());
            setChannel(DatagramChannel.open());
        }

        channel().configureBlocking(false);

        InetSocketAddress inetSocketAddress = new InetSocketAddress(getHost(), getPort());

        try {
            DatagramChannel connect = channel().connect(inetSocketAddress);
            setChannel(connect);
        } catch (UnresolvedAddressException | IOException e) {
            reportError(e.getMessage(), e);
        }
    }

    @Override
    protected boolean isConnected(DatagramChannel channel) {
        return channel.isConnected();
    }
}
