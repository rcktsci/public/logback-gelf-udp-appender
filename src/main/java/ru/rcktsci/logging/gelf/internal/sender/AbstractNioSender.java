package ru.rcktsci.logging.gelf.internal.sender;

import ru.rcktsci.logging.gelf.internal.pools.DynamicPoolStore;
import ru.rcktsci.logging.gelf.utils.Closer;
import ru.rcktsci.logging.gelf.internal.ErrorReporter;

import java.io.IOException;
import java.net.InetAddress;
import java.net.PortUnreachableException;
import java.net.UnknownHostException;
import java.nio.channels.ByteChannel;
import java.nio.channels.spi.AbstractSelectableChannel;

/**
 * Base class for NIO-channel senders.
 *
 * @param <T> must be extended by {@link AbstractSelectableChannel} and {@link ByteChannel}.
 * @since 1.11
 */
public abstract class AbstractNioSender<T extends AbstractSelectableChannel & ByteChannel> implements ErrorReporter {

    private final ErrorReporter errorReporter;
    private final String host;
    private final int port;
    private T channel;
    private volatile boolean shutdown = false;

    /**
     * Create a new {@link AbstractNioSender} given {@link ErrorReporter}, {@code host} and {@code port}. Object creation
     * triggers hostname lookup for early failure.
     *
     * @param errorReporter the error reporter.
     * @param host          hostname.
     * @param port          port number.
     */
    protected AbstractNioSender(ErrorReporter errorReporter, String host, int port) {

        // validate first address succeeds.
        try {
            InetAddress.getByName(host);
        } catch (UnknownHostException e) {
            errorReporter.reportError("Cannot resolve " + host, e);
        }
        this.errorReporter = errorReporter;
        this.host = host;
        this.port = port;

    }

    protected boolean isConnected() {
        var resource = DynamicPoolStore.getDynamicReadBufferPool().getResource();
        var byteBuffer = resource.data();

        byteBuffer.clear();
        T myChannel = channel();

        if (myChannel != null && myChannel.isOpen() && isConnected(myChannel)) {

            try {
                return myChannel.read(byteBuffer) >= 0;
            } catch (PortUnreachableException e) {
                errorReporter.reportError("Port " + getHost() + ":" + getPort() + " not reachable", e);
                Closer.close(channel());
            } catch (IOException e) {
                errorReporter.reportError("Cannot verify whether channel to " + getHost() + ":" + getPort() + " is connected: "
                        + e.getMessage(), e);
            } finally {
                DynamicPoolStore.getDynamicReadBufferPool().freeResource(resource);
            }
        }

        return false;
    }

    protected abstract boolean isConnected(T channel);

    protected T channel() {
        return channel;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public void close() {
        shutdown = true;
        Closer.close(channel());
    }

    public boolean isShutdown() {
        return shutdown;
    }

    @Override
    public void reportError(String message, Exception ex) {
        errorReporter.reportError(message, ex);
    }

    public void setChannel(T channel) {
        this.channel = channel;
    }
}
