package ru.rcktsci.logging.gelf.internal;

/**
 * Error reporter to report errors while submitting log event.
 *
 * @since 27.01.14 09:18
 */
public interface ErrorReporter {

    /**
     * Report an error caused by exception.
     *
     * @param message the message
     * @param ex      the exception
     */
    void reportError(String message, Exception ex);
}
