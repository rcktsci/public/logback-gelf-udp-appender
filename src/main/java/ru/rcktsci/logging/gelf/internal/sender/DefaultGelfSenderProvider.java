package ru.rcktsci.logging.gelf.internal.sender;

import ru.rcktsci.logging.gelf.internal.GelfSender;
import ru.rcktsci.logging.gelf.internal.GelfSenderConfiguration;
import ru.rcktsci.logging.gelf.internal.GelfSenderProvider;

import java.io.IOException;
import java.net.URI;
import java.util.Map;

/**
 * Default provider for {@link GelfSender} that creates UDP senders.
 */
public class DefaultGelfSenderProvider implements GelfSenderProvider {

    /**
     * Default GELF port.
     */
    public static final int DEFAULT_PORT = 12201;

    private static final Map<String, GelfSenderProducer> factories;

    private static final GelfSenderProducer udpSenderFactory = (configuration, host, port) -> {

        URI uri = URI.create(host);
        String udpGraylogHost = QueryStringParser.getHost(uri);
        return new GelfUDPSender(udpGraylogHost, port, configuration.getErrorReporter());
    };

    private static final GelfSenderProducer defaultSenderFactory = (configuration, host, port) -> new GelfUDPSender(host, port, configuration.getErrorReporter());

    static {
        factories = Map.of(
                "udp:", udpSenderFactory
        );
    }

    @Override
    public boolean supports(String host) {
        return host != null;
    }

    @Override
    public GelfSender create(GelfSenderConfiguration configuration) throws IOException {

        String graylogHost = configuration.getHost();

        int port = configuration.getPort();
        if (port == 0) {
            port = DEFAULT_PORT;
        }

        for (Map.Entry<String, GelfSenderProducer> entry : factories.entrySet()) {
            if (graylogHost.startsWith(entry.getKey())) {
                return entry.getValue().create(configuration, graylogHost, port);
            }
        }

        return defaultSenderFactory.create(configuration, graylogHost, port);

    }

    private interface GelfSenderProducer {

        /**
         * Produce a {@link GelfSender} using {@code configuration}, {@code host} and {@code port},
         *
         * @param configuration
         * @param host
         * @param port
         * @return
         * @throws IOException
         */
        GelfSender create(GelfSenderConfiguration configuration, String host, int port) throws IOException;
    }
}
