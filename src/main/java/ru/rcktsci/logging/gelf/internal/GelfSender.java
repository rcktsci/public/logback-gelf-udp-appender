package ru.rcktsci.logging.gelf.internal;

import java.io.Closeable;

/**
 * Strategy interface to send a {@link GelfMessage} without being opinionated about the underlying transport.
 */
public interface GelfSender extends Closeable {

    /**
     * Send the Gelf message.
     *
     * @param message the message
     * @return {@literal true} if the message was sent
     */
    boolean sendMessage(GelfMessage message);
}
