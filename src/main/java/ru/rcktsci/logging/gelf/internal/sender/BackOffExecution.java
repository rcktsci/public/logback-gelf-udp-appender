package ru.rcktsci.logging.gelf.internal.sender;

/**
 * Represent a particular back-off execution.
 *
 * <p>
 * Implementations do not need to be thread safe.
 *
 * @see BackOff
 */
interface BackOffExecution {

    /**
     * Return value of {@link #nextBackOff()} that indicates that the operation should not be retried.
     */
    long STOP = -1;

    /**
     * @return time in ms to wait before a next attempt
     */
    long nextBackOff();
}
