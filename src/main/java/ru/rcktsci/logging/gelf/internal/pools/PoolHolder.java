package ru.rcktsci.logging.gelf.internal.pools;

import ru.rcktsci.logging.gelf.internal.OutputAccessor;
import ru.rcktsci.logging.gelf.internal.ReusableGzipOutputStream;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Holder for pools.
 */
public class PoolHolder {

    private final OutputAccessor.OutputAccessorPoolHolder outputAccessorPoolHolder;

    private final ReusableGzipOutputStream streamPool;

    private final ByteBuffer[] singleBufferPool = new ByteBuffer[1];

    private final byte[] byteArrayPool = new byte[8192 * 2];

    private PoolHolder() {
        this(new OutputAccessor.OutputAccessorPoolHolder());
    }

    private PoolHolder(OutputAccessor.OutputAccessorPoolHolder outputAccessorPoolHolder) {
        this.outputAccessorPoolHolder = outputAccessorPoolHolder;
        try {
            this.streamPool = new ReusableGzipOutputStream(OutputAccessor.pooledStream(outputAccessorPoolHolder));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static PoolHolder threadLocal() {
        return new PoolHolder();
    }

    /**
     * @return the {@link OutputAccessor.OutputAccessorPoolHolder}.
     */
    public OutputAccessor.OutputAccessorPoolHolder getOutputAccessorPoolHolder() {
        return outputAccessorPoolHolder;
    }

    /**
     * @return a pooled {@link ReusableGzipOutputStream} instance.
     */
    public ReusableGzipOutputStream getReusableGzipOutputStream() {
        return streamPool;
    }

    /**
     * @return a pooled {@link ByteBuffer}-array instance.
     */
    public ByteBuffer[] getSingleBuffer() {
        return singleBufferPool;
    }

    /**
     * @return a pooled byte-array instance.
     */
    public byte[] getByteArray() {
        return byteArrayPool;
    }
}
