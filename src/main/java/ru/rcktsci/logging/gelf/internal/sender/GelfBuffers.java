package ru.rcktsci.logging.gelf.internal.sender;

import ru.rcktsci.logging.gelf.internal.GelfMessage;

import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;

/**
 * Utility to create UDP buffers from a {@link GelfMessage} by working with {@link ByteBuffer}. Buffers are provided by
 * {@link ru.rcktsci.logging.gelf.internal.pools.DynamicPoolStore}.
 */
class GelfBuffers {

    private GelfBuffers() {
        // no instance allowed
    }

    /**
     * Create UDP buffers and apply auto-buffer-enlarging, if necessary.
     *
     * @param message
     * @param writeBuffers
     * @param tempBuffers
     * @return
     */
    protected static ByteBuffer[] toUDPBuffers(GelfMessage message, ByteBuffer writeBuffers, ByteBuffer tempBuffers) {
        ByteBuffer workingWriteBuffer = writeBuffers;
        ByteBuffer workingTempBuffer = tempBuffers;

        while (true) {
            try {
                return message.toUDPBuffers(workingWriteBuffer.clear(), workingTempBuffer.clear());
            } catch (BufferOverflowException e) {
                workingWriteBuffer = getLargestBuffer(workingWriteBuffer);
                workingTempBuffer = getLargestBuffer(workingTempBuffer);
            }
        }
    }

    private static ByteBuffer getLargestBuffer(ByteBuffer buffers) {
        return ByteBuffer.allocateDirect(calculateNewBufferSize(buffers.capacity()));
    }

    private static int calculateNewBufferSize(int capacity) {
        return (int) (capacity * 1.5);
    }
}
