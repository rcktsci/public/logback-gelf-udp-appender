package ru.rcktsci.logging.gelf.internal.pools;

import lombok.Getter;

import ru.rcktsci.logging.gelf.configuration.RuntimeProperties;

import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.UUID;

public class DynamicPoolStore {

    @Getter
    private static final DynamicPool<PoolHolder> dynamicPoolHolder;
    @Getter
    private static final DynamicPool<ResourceData<ByteBuffer>> dynamicByteBufferPool;
    @Getter
    private static final DynamicPool<ResourceData<ByteBuffer>> dynamicReadBufferPool;

    static {
        dynamicPoolHolder = DynamicPool.create(PoolHolder::threadLocal);
        dynamicByteBufferPool = DynamicPool.create(
                () -> new ResourceData<>(ByteBuffer.allocateDirect(RuntimeProperties.getBufferSizeProperty()))
        );
        dynamicReadBufferPool = DynamicPool.create(
                () -> new ResourceData<>(ByteBuffer.allocate(1))
        );
    }

    public record ResourceData<T>(UUID id, T data) {

        public ResourceData(T data) {
            this(UUID.randomUUID(), data);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            ResourceData<?> that = (ResourceData<?>) o;
            return id.equals(that.id);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }
    }
}
