package ru.rcktsci.logging.gelf.internal.pools;

import ru.rcktsci.logging.gelf.configuration.RuntimeProperties;

import java.util.Arrays;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

public class DynamicPool<T> {

    private final Queue<T> queue = new ConcurrentLinkedQueue<>();
    private final ThreadLocal<T> contextResource = new ThreadLocal<>();

    private long[] resourceUsageStatistics = new long[RuntimeProperties.getGelfDynamicPoolStatisticsSize()];
    private int resourceUsageStatisticsIndex = 0;

    private final AtomicInteger totalQueueSize = new AtomicInteger(0);
    private final AtomicInteger lastRangeUsageCount = new AtomicInteger(0);
    private final AtomicInteger lastRangeMaxUsageCount = new AtomicInteger(0);

    private final Supplier<T> creator;

    private DynamicPool(Supplier<T> creator) {
        this.creator = creator;
        Arrays.fill(resourceUsageStatistics, -1);

        var poolOptimizer = new Thread(this::poolCleanWorker);
        poolOptimizer.setDaemon(true);
        poolOptimizer.start();
    }

    public static <T> DynamicPool<T> create(Supplier<T> creator) {
        return new DynamicPool<>(creator);
    }

    public void executeWithinResourceContext(Runnable function) {
        T resource = getResource();
        try {
            contextResource.set(resource);
            function.run();
        } finally {
            contextResource.remove();
            freeResource(resource);
        }
    }

    public T getContextResource() {
        T result = contextResource.get();
        Objects.requireNonNull(result, "This method should be called inside the resource context!");
        return result;
    }

    public T getResource() {
        T existing = queue.poll();

        var newUsage = lastRangeUsageCount.incrementAndGet();
        var lastMaxUsage = lastRangeMaxUsageCount.get();
        if (lastMaxUsage < newUsage) {
            lastRangeMaxUsageCount.set(newUsage);
        }

        if (existing == null) {
            totalQueueSize.incrementAndGet();
            return creator.get();
        }

        return existing;
    }

    public void freeResource(T resource) {
        queue.add(resource);
        lastRangeUsageCount.decrementAndGet();
    }

    private void poolCleanWorker() {
        while (true) {
            try {
                runCleaner();
            } catch (InterruptedException ex) {
                break;
            } catch (Exception ignored) {
            }
        }
    }

    private void runCleaner() throws InterruptedException {
        TimeUnit.SECONDS.sleep(RuntimeProperties.getGelfResourcePoolOptimizeTimeRange());

        var currentPoolStatisticsSize = RuntimeProperties.getGelfDynamicPoolStatisticsSize();
        var oldPoolStatisticsSize = resourceUsageStatistics.length;

        if (currentPoolStatisticsSize != oldPoolStatisticsSize) {
            var newUsageStatistics = new long[currentPoolStatisticsSize];

            if (currentPoolStatisticsSize > oldPoolStatisticsSize) {
                System.arraycopy(resourceUsageStatistics,
                                 0,
                                 newUsageStatistics,
                                 0,
                                 oldPoolStatisticsSize);

                for (int i = oldPoolStatisticsSize; i < currentPoolStatisticsSize; i++) {
                    newUsageStatistics[i] = -1;
                }
            } else {
                int reversedIndex = resourceUsageStatisticsIndex;

                for (int i = 0; i < currentPoolStatisticsSize; i++) {
                    newUsageStatistics[i] = resourceUsageStatistics[reversedIndex];
                    reversedIndex = reversedIndex - 1 < 0
                                    ? resourceUsageStatistics.length - 1
                                    : reversedIndex - 1;
                }

                resourceUsageStatisticsIndex = 1;
            }

            resourceUsageStatistics = newUsageStatistics;
        }

        cleaner();
    }

    private void cleaner() {
        lastRangeUsageCount.set(0);
        resourceUsageStatistics[resourceUsageStatisticsIndex] = lastRangeMaxUsageCount.getAndSet(0);

        int totalUsage = 0;
        int totalFilledRangesCount = 0;
        for (var usageAtRange : resourceUsageStatistics) {
            if (usageAtRange != -1) {
                totalUsage += usageAtRange;
                totalFilledRangesCount++;
            }
        }
        int mediumUsage = totalUsage / totalFilledRangesCount;
        int usageAdvantage = totalQueueSize.get() - mediumUsage;

        if (usageAdvantage > 0 && !queue.isEmpty()) {
            for (int i = 0; i < queue.size(); i++) {
                if(queue.poll() != null) {
                    totalQueueSize.decrementAndGet();
                }
            }
        }

        resourceUsageStatisticsIndex = (resourceUsageStatisticsIndex + 1) % resourceUsageStatistics.length;
    }
}
